<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use HasFactory;
    
    // uso de softdelete para eliminar customer de manera lógica
    use SoftDeletes;

    // nombre de la tabla que usa el modelo
    protected $table = "customers";
    protected $primaryKey = "dni";

    // datos
    protected $fillable = [
        'dni',
        'id_reg',
        'id_com',
        'email',
        'name',
        'last_name',
        'address',
        'date_reg',
        'status'
    ];

    protected $timestamp = false;

    // relaciones
    public function communes()
    {
        return $this->belongsTo(Communes::class);
    }
}
