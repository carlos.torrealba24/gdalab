<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    use HasFactory;

    // nombre de la tabla que usa el modelo
    protected $table = "access_tokens";

    // datos
    protected $fillable = [
        'token',
        'expired_at'
    ];
    protected $timestamp = false;
}
