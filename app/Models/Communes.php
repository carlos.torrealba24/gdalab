<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Communes extends Model
{
    use HasFactory;
    use SoftDeletes;

    // nombre de la tabla que usa el modelo
    protected $table = "communes";
    protected $primaryKey = "id_com";

    // datos
    protected $fillable = [
        'id_reg',
        'description',
        'status'
    ];
    protected $timestamp = false;


    // Relaciones
    public function regions()
    {
        return $this->belongsTo(Regions::class, 'id_reg');
    }

    public function customers()
    {
        return $this->hasMany(Customers::class);
    }
}
