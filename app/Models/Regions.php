<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regions extends Model
{
    use HasFactory;
    use SoftDeletes;

    // nombre de la tabla que usa el modelo
    protected $table = "regions";
    protected $primaryKey = "id_reg";

    // datos
    protected $fillable = [
        'description',
        'status'
    ];

    protected $timestamp = false;

    // relaciones
    public function communes()
    {
        return $this->hasMany(Communes::class, 'id_reg');
    }
}
