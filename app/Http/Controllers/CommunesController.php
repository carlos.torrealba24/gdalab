<?php

namespace App\Http\Controllers;

use App\Models\Communes;
use Illuminate\Http\Request;

class CommunesController extends Controller
{
    // Registrar una communes
    public function register(Request $request){
        // validacion de datos
        $request->validate([
            'id_reg' => 'required',
            'description' => 'required',
            'status' => 'required'
        ]);
        // Proceso de registro
        try{
            $communes = new Communes;
            $communes->id_reg = $request->id_reg;
            $communes->description = $request->description;
            $communes->status = $request->status;
            $communes->save();
            return response()->json(["success" => true, "data" => $communes ]);
        }
        catch(\Exception $e){
            // do task when error
            return response()->json(['success' => 'false', 'mensaje' => 'Error' ]);    
        }
        
    }
}