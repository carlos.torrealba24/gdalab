<?php

namespace App\Http\Controllers;

use App\Models\Regions;
use Illuminate\Http\Request;

class RegionsController extends Controller
{

    public function register(Request $request){
        $request->validate([
            'description' =>'required|max:90',
            'status' => 'required'
        ]);
        try{
            $regions = new Regions;
            $regions->description = $request->description;
            $regions->status = $request->status;
            $regions->save();
            return response()->json(["success" => true, "data" => $regions ]);
        }
        catch(\Exception $e){
            // do task when error
            return response()->json(['success' => 'False' ]);    // insert query ARREGLAAARRR
        }
        
    }
}
