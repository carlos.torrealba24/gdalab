<?php

namespace App\Http\Controllers;

use App\Models\Regions;
use App\Models\Communes;
use App\Models\Customers;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    // registrar customers
    public function register(Request $request){
        // validación de datos
        $request->validate([
            'dni' => 'required',
            'id_reg' => 'required',
            'id_com' =>'required',
            'email' => 'required|email|max:120',
            'name' => 'required|max:45',
            'last_name' => 'required|max:45',
            'address'  => 'required|max:255',
            'date_reg' => 'required',
            'status' => 'required'
            
        ]);
        // validar que existan communes y regions
        try {
            $communes = Communes::find($request->id_com);
            $regions = Regions::find($request->id_reg);
    
            if(!$communes){
                return response()->json(["success" => false, 'mensaje' => 'La commune no existe' ]);
            }
    
            if(!$regions){
                return response()->json(["success" => false, 'mensaje' => 'La region no existe' ]);
            }
            
            // validar que el id de las regiones coincidan para comenzar registro
            if($communes->id_reg == $request->id_reg){
                $customers = new Customers;
                $customers->dni = $request->dni;
                $customers->id_reg = $request->id_reg;
                $customers->id_com = $request->id_com;
                $customers->email = $request->email;
                $customers->name = $request->name;
                $customers->last_name = $request->last_name;
                $customers->address = $request->address;
                $customers->date_reg = $request->date_reg;
                $customers->status = $request->status;
                $customers->save();
                return response()->json(["success" => true, 'data' => $customers ]);
            }else{
                return response()->json(["success" => false, 'mensaje' => 'La comuna no pertenece a la region' ]);
            }
        } catch (\Throwable $th) {
            return response()->json(["success" => false, 'mensaje' => 'Error' ]);

        }
    }
    

    // consultar customer por el dni
    public function consultByDni($dni){

        $customers = Customers::where([
            ['dni', '=', $dni],
            ['status', '=', 'A']
        ])->get();
        if($customers){
            return response()->json(["success" => true, "data" => $customers ]);
        }else{
            return response()->json(['success' => 'False', 'mensaje' => 'No se encontró el customer' ]);
        }
    }


    // consultar customer por el correo
    public function consultByEmail($email){
        $customers = Customers::where([
            ['dni', '=', $email],
            ['status', '=', 'A']
        ])->get();
        if($customers){
            return response()->json(["success" => true, "data" => $customers ]);
        }else{
            return response()->json(['success' => 'False', 'mensaje' => 'No se encontró el customer' ]);
        }
    }
    

    // Eliminación lógica utilizando SoftDelete
    public function logicDelete($dni){
        $customers = Customers::where('dni', '=', $dni)->first();
        
        if($customers->trashed()){
            return response()->json(["success" => true, 'mensaje' => 'Regitro no existe' ]);
        }else{
            $customers = Customers::where('dni', '=', $dni)->delete();
            return response()->json(["success" => true, 'mensaje' => 'Registro eliminado' ]);
        }
    }
}
