<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\AccessTokenController;

class UserController extends Controller
{
    public function register(Request $request){
        $request->validate([
            'email' => 'required',
            'name' => 'required',
            'password' => 'required'
        ]);
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $accesstoken = AccessTokenController::generate($request->email);
            $user->save();
            return response()->json(["success" => true, 'mensaje' => 'Usuario registrado', 'token' => $accesstoken ]);
        } catch (\Throwable $th) {
            return response()->json(["success" => true, 'mensaje' => $th->getMessage() ]);
        }
       
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        
        $user = User::where('email', '=', $request->email)->first();

        if(!$user){
            return response()->json(["success" => false, 'mensaje' => 'Usuario no existe' ]);
        }

        if(!Hash::check($request->password, $user->password)){
            return response()->json(["success" => false, 'mensaje' => 'Password incorrecta' ]);
        }

        return response()->json(["success" => true, "mensaje", "Ingreso satisfactorio"]);
    }
}
