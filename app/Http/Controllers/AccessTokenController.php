<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use Illuminate\Http\Request;

class AccessTokenController extends Controller
{
    static public function  generate($email){

        // crear token
        try {
            $now = \Carbon\Carbon::now();
            $time = time();
            $randomNumbers = range(200, 500);
            $randomNumberValue = array_rand($randomNumbers, 1);
            $str = \Str::random($randomNumbers[$randomNumberValue]);
            $withOutHashed = "$email-$time";
            $expired = $now->addHours(5);
            $hash = hash("sha1", $withOutHashed)."-$str";
            AccessToken::create([
                "token" => $hash,
                "expired_at" => $expired
            ]);
            return response()->json(["success" => true, "token" => $hash ]);
        } catch (\Throwable $th) {
            return response()->json(["success" => false, "data" => 'Error' ]);

        }
    }
}
