<?php

namespace App\Http\Middleware;

use App\Models\AccessToken;
use Closure;
use Illuminate\Http\Request;

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        /*  Validar que el token que viene por el header desde el postman
            sea igual al que hay en la base de datos y además verificar
            que el token no haya expirado.
        */
        $now = \Carbon\Carbon::now();
        $accessToken = AccessToken::latest()->first();
        if (($request->header('Authorization') !== $accessToken->token) && ($now <= $accessToken->expired_at)) {
            return $next($request);
        }else{
            return response()->json(["success" => false, "data" => 'Error de autenticación' ]);
        }
        
    }
}
