<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;

class CustomersController extends Controller
{

    public function register(Request $request){
        $request->validate([
            'dni' => 'required',
            'id_reg' => 'required',
            'id_com' =>'required',
            'email' => 'required|email|max:120',
            'name' => 'required|max:45',
            'last_name' => 'required|max:45',
            'address'  => 'required|max:255',
            
        ]);
        try{
            $customers = new Customers;
            $customers->dni = $request->dni;
            $customers->id_reg = $request->id_reg;
            $customers->id_com = $request->id_com;
            $customers->email = $request->email;
            $customers->name = $request->name;
            $customers->last_name = $request->last_name;
            $customers->address = $request->address;
            //$customers->save();
            return response()->json(["mensaje" => $customers ]);
        }
        catch(\Exception $e){
            // do task when error
            return response()->json(['success' => $e->getMessage() ]);    // insert query ARREGLAAARRR
        }
    }
    
    public function consultByDni($dni){
        $customers = Customers::where([
            ['dni', '=', $dni],
            ['status', '=', 'A']
        ])->get();
        if($customers){
            return response()->json(["success" => $customers ]);
        }else{
            return response()->json(['success' => 'False' ]);    // insert query ARREGLARRRR
        }
    }

    public function consultByEmail($email){
        $customers = Customers::where([
            ['email', '=', $email],
            ['status', '=', 'A'],            
        ])->get();
        if($customers){
            return response()->json(["success" => $customers ]);
        }else{
            return response()->json(['success' => 'False' ]);    // insert query ARREGLARRRR
        }
    }

    public function delete($dni){
        $customers = Customers::where([
            ['dni', '=', $dni],
            ['status', '=', 'A'],
        ])->first();
        
    }
}
