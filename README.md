///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Se entiende por API como una interface de programación de desarrollo
y tiene como finalidad permitir la comunicación entre dos o más componentes.

Esta API fué diseñada para probar que se pueden registrar la siguiente información:

    - Regions
    - Communes
    - Customer

Además por cada customer se puede consultar su información mediante su DNI o su EMAIL
y por ultimo el customer tambien puede ser eliminado.



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para instalación del proyecto:

1. Ingresar desde consola a la carpeta htdocs de su servidor.
2. Clonar el proyecto con https.
3. Abrir proyecto en el IDE de preferencia.
4. Modificar archivo `.env` la opción `app_debug` colocar `false` y la opción `DB_DATABASE` colocar el nombre de la bd.
5. Abrir una terminal con el directorio raiz del proyecto y ejecutar el siguiente comando `php artisan migrate` para crear las tablas en la bd.
6. Luego de terminar las migraciones se debe correr el servidor ejecutando el siguiente comando `php artisan serve`.
7. Copiar url del servidor, se utilizara más adelante.
8. Una vez ya levantado el servidor se procede a abrir postman.



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Archivos modificados y creados:

Archivos en `app/http/controllers`:

    - AccessTokenController
    - CommunesController
    - CustomersController
    - RegionsController
    - UserController

Archivos en `app/http/middleware`:

    - EnsureTokenIsValid

Archivos en `app/http/models`:

    - AccessToken
    - Communes
    - Customers
    - Regions
    - User

Archivos en `database/migrations`:
    - Migracion de accessToken
    - Migracion de regions
    - Migracion de customers
    - Migracion de communes


Archivo `app/http/kernel.php`
Archivo `routes/api.php`

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para utilizar y probar la API:

1. Al ingresar a postman debe crear una nueva collection.
2. Se crea la primera petición para crear un usuario, colocando el tipo de petición en `POST`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/register-user`.
    
    - Para realizar esta petición debe ingresar los datos por `Body` y luego seleccionar `form-data`.
    - Una vez estando en `form-data` debe ingresar las siguientes `Keys` junto a sus `Valores`:
    - Las key son las siguientes:
        - name
        - email
        - password
    - Presionar `SEND`.
3. Al crear el registro deberia mostrarle por consola un mensaje de usuario registrado y un `Token`.
4. Debe copiar el `token` e ir al postman.
5. Ingresar a las opciones de editar de la `Collection` ya creada en el postman.
6. Seleccionar pestaña `Variables` y crear una variable `accessToken` cuyo `current value` será el `token` antes copiado.
7. Es importante saber que por cada petición se debe agregar un `Header`, para ello debe crear una `Key` en `Header`
   llamada `Authorization` cuyo valor tendra `{{accessToken}}`.



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para crear una petición de usar el login:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `POST`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/login`.
        - Para realizar esta petición debe ingresar los datos por `Body` y luego seleccionar `form-data`.
        - Una vez estando en `form-data` debe ingresar las siguientes `Keys` junto a sus `Valores`:
        - Las key son las siguientes:
            - email
            - password
        - Presionar `SEND`.
2. `NOTAAA`: Si al intentar loguearse pueda dar error y puede ser por las siguientes dos cosas:

    - Email o password incorrectos.
    - `Token` expirado, para este caso debe crear un nuevo `Token` para ello debe hacer lo siguiente:

        - Agregar otra pestaña nueva y colocar el tipo de petición en `GET`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
            ejemplo: `http://127.0.0.1.8000/proyecto/public/api/access-token-generate/carlos@gmail.com`.
            se debe pasar por la url de la petición un email valido.
        - Proceder con el item `5` de la anterior sección y cambiar el `current value` de la variable `accessToken`.




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para crear una petición de crear una regions:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `POST`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/regions/register`.
        - Para realizar esta petición debe ingresar los datos por `Body` y luego seleccionar `form-data`.
        - Una vez estando en `form-data` debe ingresar las siguientes `Keys` junto a sus `Valores`:
        - Las key son las siguientes:
            - description
            - status  `cuyo valor puede ser A ó I`.
        - Presionar `SEND`.
2. Se debera ver en consola un `success` `true` y la `data` registrada.
3. Cabe destacar que cada `region` posee un `Id` llamado `id_reg` pero este no se asigna ya que se autoincrementa,
    por lo que comienza en el valor `1`.



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para crear una petición de crear una commune:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `POST`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/communes/register`.
        - Para realizar esta petición debe ingresar los datos por `Body` y luego seleccionar `form-data`.
        - Una vez estando en `form-data` debe ingresar las siguientes `Keys` junto a sus `Valores`:
        - Las key son las siguientes:
            - description
            - id_reg  `id de la region que pertenece`.
            - status  `cuyo valor puede ser A ó I`.
        - Presionar `SEND`.
2. Se debera ver en consola un `success` `true` y la `data` registrada.
3. Cabe destacar que cada `commune` posee un `Id` llamado `id_com` pero este no se asigna ya que se autoincrementa,
    por lo que comienza en el valor `1`.



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para crear una petición de crear un customer:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `POST`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/customers/register`.
        - Para realizar esta petición debe ingresar los datos por `Body` y luego seleccionar `form-data`.
        - Una vez estando en `form-data` debe ingresar las siguientes `Keys` junto a sus `Valores`:
        - Las key son las siguientes:
            - dni
            - id_reg  `id de la region que pertenece`.
            - id_com  `id de la commune que pertenece`.
            - email
            - name
            - last_name
            - address
            - date_reg `formato de fecha puede ser dd-mm-yyyy`
            - status  `cuyo valor puede ser A ó I`.
        - Presionar `SEND`.
2. Se debera ver en consola un `success` `true` y la `data` registrada.
3. Si la commune no pertenece a la región esto retornara por consola un error. (validación).



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para consultar un customer por medio del dni:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `GET`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/customers/consultdni/1`.
    se debe pasar por la url de la petición un dni valido.
2. Se debera ver en consola un `success` `true` y la `data` registrada.



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para consultar un customer por medio del dni:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `GET`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/customers/consultdni/1`.
    se debe pasar por la url de la petición un `dni` valido.
2. Se debera ver en consola un `success` `true` y la `data` registrada.
3. De colocar un dni no valido mostrará un error (validación).



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para consultar un customer por medio del email:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `GET`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/customers/consultemail/1`.
    se debe pasar por la url de la petición un `email` valido.
2. Se debera ver en consola un `success` `true` y la `data` registrada.
3. De colocar un email no valido mostrará un error (validación).



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pasos para eliminar un customer:

1. Agregar otra pestaña nueva y colocar el tipo de petición en `DELETE`+`URL SERVIDOR`/`URL PROYECTO`/`peticion`
    ejemplo: `http://127.0.0.1.8000/proyecto/public/api/customers/logicdelete/1`.
    se debe pasar por la url de la petición un `dni` valido.
2. Se debera ver en consola un `success` `true` y un `mensaje`.
3. La api tiene una validación que solo se pueden eliminar customers con `status = A` o `status = I`,
    en caso que el customer ya esté eliminado y se intenta eliminar de nuevo mostrara un `mensaje` (validación).

