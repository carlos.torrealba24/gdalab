<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->string('dni',45);
            $table->integer('id_reg')->unsigned();
            $table->integer('id_com')->unsigned();
            $table->string('email',120)->nullable();
            $table->string('name',45)->nullable();
            $table->string('last_name',45)->nullable();
            $table->string('address',255)->nullable();
            $table->dateTime('date_reg', $precision = 0)->nullable();
            $table->enum('status', ['A', 'I', 'T']);
            $table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
            $table->foreign('id_reg')->references('id_reg')->on('regions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_com')->references('id_com')->on('communes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
