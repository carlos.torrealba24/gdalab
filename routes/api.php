<?php

use App\Http\Controllers\AccessTokenController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegionsController;
use App\Http\Controllers\CommunesController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//auth routes
Route::get('/access-token-generate/{email}', [AccessTokenController::class, "generate"]);
Route::post('/login', [UserController::class, "login"]);
Route::post('/register-user', [UserController::class, "register"]);


//costumers routes
Route::post('/customers/register', [CustomersController::class, "register"]);
Route::get('/customers/consultdni/{dni}', [CustomersController::class, "consultByDni"]);
Route::get('/customers/consultemail/{email}', [CustomersController::class, "consultByEmail"]);
Route::delete('/customers/logicdelete/{dni}', [CustomersController::class, "logicDelete"]);


//regions routes
Route::post('/regions/register', [RegionsController::class, 'register']);


//communes routes
Route::post('/communes/register', [CommunesController::class, "register"]);




/* RUTAS CON MIDDLEWARE
//auth routes
Route::get('/access-token-generate/{email}', [AccessTokenController::class, "generate"])->middleware('validateToken');
Route::post('/login', [UserController::class, "login"])->middleware('validateToken');
Route::post('/register-user', [UserController::class, "register"])->middleware('validateToken');


//costumers routes
Route::post('/customers/register', [CustomersController::class, "register"])->middleware('validateToken');
Route::get('/customers/consultdni/{dni}', [CustomersController::class, "consultByDni"])->middleware('validateToken');
Route::get('/customers/consultemail/{email}', [CustomersController::class, "consultByEmail"])->middleware('validateToken');
Route::delete('/customers/logicdelete/{dni}', [CustomersController::class, "logicDelete"])->middleware('validateToken');


//regions routes
Route::post('/regions/register', [RegionsController::class, 'register'])->middleware('validateToken');


//communes routes
Route::post('/communes/register', [CommunesController::class, "register"])->middleware('validateToken');*/